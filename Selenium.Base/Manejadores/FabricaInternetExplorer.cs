﻿// <copyright file="FabricaInternetExplorer.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base.Manejadores
{
    using OpenQA.Selenium.IE;
    using OpenQA.Selenium.Remote;

    /// <summary>
    /// Fabrica de manejadores específica para Microsoft Internet Explorer.
    /// </summary>
    internal class FabricaInternetExplorer : IFabrica
    {
        /// <summary>
        /// Crea un manejador para el navegador web Microsoft Internet Explorer.
        /// </summary>
        /// <returns>Un manejador para Microsoft Internet Explorer ya configurado.</returns>
        public RemoteWebDriver CrearManejador()
        {
            return new InternetExplorerDriver(@"D:\WebDrivers\");
        }
    }
}
