﻿// <copyright file="FabricaEdge.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base.Manejadores
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Edge;
    using OpenQA.Selenium.Remote;

    /// <summary>
    /// Fábrica de manejadores específica para Microsoft Edge.
    /// </summary>
    internal class FabricaEdge : IFabrica
    {
        /// <summary>
        /// Crea un manejador para el navegador web Microsoft Edge.
        /// </summary>
        /// <returns>Un manejador para Microsoft Edge ya configurado.</returns>
        public RemoteWebDriver CrearManejador()
        {
            EdgeOptions opciones = new EdgeOptions
            {
                PageLoadStrategy = PageLoadStrategy.Normal,
            };
            return new EdgeDriver(@"D:\WebDrivers\", opciones);
        }
    }
}
