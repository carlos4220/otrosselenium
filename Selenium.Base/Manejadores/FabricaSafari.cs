﻿// <copyright file="FabricaSafari.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base.Manejadores
{
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Safari;

    /// <summary>
    /// Fabrica de manejadores específica para Apple Safari.
    /// </summary>
    internal class FabricaSafari : IFabrica
    {
        /// <summary>
        /// Crea un manejador para el navegador web Apple Safari.
        /// </summary>
        /// <returns>Un manejador para Apple Safari ya configurado.</returns>
        public RemoteWebDriver CrearManejador()
        {
            return new SafariDriver();
        }
    }
}
