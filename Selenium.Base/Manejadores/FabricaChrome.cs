﻿// <copyright file="FabricaChrome.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base.Manejadores
{
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Remote;

    /// <summary>
    /// Fabrica de manejadores específica para Google Chrome.
    /// </summary>
    internal class FabricaChrome : IFabrica
    {
        /// <summary>
        /// Crea un manejador para el navegador web Google Chrome.
        /// </summary>
        /// <returns>Un manejador para Google Chrome ya configurado.</returns>
        public RemoteWebDriver CrearManejador()
        {
            ChromeOptions  options = new ChromeOptions();
            options.AddArgument("no-sandbox");
            return new ChromeDriver(@"D:\WebDrivers\chromedriver", options,System.TimeSpan.FromMinutes(10));

            //var opciones = new ChromeOptions();
            //opciones.AddArgument("headless");
        }
    }
}
