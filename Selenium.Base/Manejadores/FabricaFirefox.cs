﻿// <copyright file="FabricaFirefox.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base.Manejadores
{
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Remote;

    /// <summary>
    /// Fabrica de manejadores específica para Mozilla Firefox.
    /// </summary>
    internal class FabricaFirefox : IFabrica
    {
        /// <summary>
        /// Crea un manejador para el navegador web Mozilla Firefox.
        /// </summary>
        /// <returns>Un manejador para Mozilla Firefox ya configurado.</returns>
        public RemoteWebDriver CrearManejador()
        {
            FirefoxOptions options = new FirefoxOptions();
            options.AddArguments("no-sandbox");
            return new FirefoxDriver(@"D:\WebDrivers\FireFoxDriver", options, System.TimeSpan.FromMinutes(10));
        }
    }
}
