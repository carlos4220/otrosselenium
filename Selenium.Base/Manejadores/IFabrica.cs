﻿// <copyright file="IFabrica.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base.Manejadores
{
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Remote;

    /// <summary>
    /// Define las operaciones que deben implementar las fábricas de manejadores de navegadores web.
    /// </summary>
    internal interface IFabrica
    {
        /// <summary>
        /// Crea un manejador para un navegador web específico.
        /// </summary>
        /// <returns>El manejador web para el navegador al que corresponde la fábrica.</returns>
        RemoteWebDriver CrearManejador();
    }
}
