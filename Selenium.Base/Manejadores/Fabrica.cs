﻿// <copyright file="Fabrica.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base.Manejadores
{
    using System;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;

    /// <summary>
    /// Provee métodos para instanciar manejadores para diferentes navegadores web.
    /// </summary>
    public class Fabrica
    {
        /// <summary>
        /// Crea una instancia de un manejador para un navegador web específico.
        /// </summary>
        /// <param name="navegador">El navegador web para el que se requiere un manejador. Este determina en qué navegador serán ejecutadas
        /// las pruebas.</param>
        /// <returns>Un manejador para un navegador web específico.</returns>
        public RemoteWebDriver CrearManejador(ListaNavegadores navegador)
        {
            // Esto asegura que al menos se cree un manejador para Google Chrome por defecto.
            IFabrica fabrica = new FabricaChrome();

            if (navegador == ListaNavegadores.Edge)
            {
                fabrica = new FabricaEdge();
            }

            if (navegador == ListaNavegadores.Firefox)
            {
                fabrica = new FabricaFirefox();
            }

            if (navegador == ListaNavegadores.InternetExplorer)
            {
                fabrica = new FabricaInternetExplorer();
            }

            if (navegador == ListaNavegadores.Safari)
            {
                fabrica = new FabricaSafari();
            }

            return fabrica.CrearManejador();
        }

        /// <summary>
        /// Crea un elemento de espera para bloquear la ejecución de las instrucciones de Selenium mientras cargan las páginas.
        /// </summary>
        /// <param name="manejador">El manejador que tendrá que bloquearse mientras carga la página.</param>
        /// <param name="tiempoEspera">El tiempo de espera máximo antes de generar un error de tiempo de espera agotado.</param>
        /// <returns>Un elemento de espera asociado a un manejador específico.</returns>
        public WebDriverWait CrearPausa(RemoteWebDriver manejador, TimeSpan tiempoEspera)
        {
            return new WebDriverWait(manejador, tiempoEspera);
        }
    }
}
