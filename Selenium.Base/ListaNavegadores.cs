﻿// <copyright file="ListaNavegadores.cs" company="Canella S.A.">
// Canella S.A., todos los derechos reservados.
// </copyright>

namespace Selenium.Base
{
    using System;

    /// <summary>
    /// Simula una "enumeración de strings" para permitir seleccionar un enumerador a partir de una cadena de texto. Un ejemplo es si se
    /// guarda el nombre del navegador para la prueba en un archivo de configuraciones.
    /// </summary>
    public sealed class ListaNavegadores
    {
        /// <summary>
        /// El elemento que representa a Google Chrome.
        /// </summary>
        public static readonly ListaNavegadores Chrome = new ListaNavegadores("Chrome");

        /// <summary>
        /// El elemento que representa a Microsoft Edge.
        /// </summary>
        public static readonly ListaNavegadores Edge = new ListaNavegadores("Edge");

        /// <summary>
        /// El elemento que representa a Mozilla Firefox.
        /// </summary>
        public static readonly ListaNavegadores Firefox = new ListaNavegadores("Firefox");

        /// <summary>
        /// El elemento que representa a Microsoft Internet Explorer.
        /// </summary>
        public static readonly ListaNavegadores InternetExplorer = new ListaNavegadores("Internet Explorer");

        /// <summary>
        /// El elemento que representa a Apple Safari.
        /// </summary>
        public static readonly ListaNavegadores Safari = new ListaNavegadores("Safari");

        /// <summary>
        /// El nombre del navegador.
        /// </summary>
        private readonly string nombre;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="ListaNavegadores"/>.
        /// </summary>
        /// <param name="nombre">El nombre del navegador web.</param>
        private ListaNavegadores(string nombre)
        {
            this.nombre = nombre;
        }

        /// <summary>
        /// Permite la conversión explícita desde una cadena de texto hacia un elemento específico de la lista de navegadores disponibles.
        /// Se incluyó para los casos en los que se use algún medio para guardar configuraciones y el nombre del navegador sólo se pueda
        /// guardar como una cadena de texto.
        /// </summary>
        /// <param name="navegador">El navegador representado por la cadena de texto. Si no corresponde con algún navegador disponible se
        /// lanza una excepción de tipo <see cref="System.InvalidCastException"/>.</param>
        public static explicit operator ListaNavegadores(string navegador)
        {
            if (string.IsNullOrEmpty(navegador))
            {
                throw new ArgumentException("El nombre del navegador no puede ser una cadena nula o vacía.", "navegador");
            }

            switch (navegador.ToUpper())
            {
                case "CHROME":
                    return Chrome;
                case "EDGE":
                    return Edge;
                case "FIREFOX":
                    return Firefox;
                case "IE":
                case "INTERNETEXPLORER":
                case "INTERNET EXPLORER":
                    return InternetExplorer;
                case "SAFARI":
                    return Safari;
                default:
                    throw new InvalidCastException("El nombre del navegador no es válido.");
            }
        }

        /// <summary>
        /// Obtiene una cadena de texto que representa al navegador.
        /// </summary>
        /// <returns>Una cadena de texto que describe el navegador.</returns>
        public override string ToString()
        {
            return this.nombre;
        }
    }
}
