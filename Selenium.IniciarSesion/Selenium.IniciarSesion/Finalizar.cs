﻿namespace Selenium.IniciarSesion
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;

    public class Finalizar
    {
        public static void FinalizaSesion(RemoteWebDriver manejador, WebDriverWait pausa, string usuario)
        {
            //pausa.Until(m => m.FindElement(By.LinkText("Logoff " + usuario)));
            //manejador.FindElement(By.LinkText("Logoff " + usuario)).Click();
            pausa.Until(m => m.FindElement(By.XPath($"//a[contains(text(),'Logoff {usuario}')]")));
            manejador.FindElement(By.XPath($"//a[contains(text(),'Logoff {usuario}')]")).Click();
        }
        public static void FinalizaSesionClienteBB(RemoteWebDriver manejador, WebDriverWait pausa)
        {
            manejador.SwitchTo().DefaultContent();
            pausa.Until(m => m.FindElement(By.LinkText("b")));
            manejador.FindElement(By.LinkText("b")).Click();
            System.Threading.Thread.Sleep(10000);
            pausa.Until(m => m.FindElement(By.Id("usercode")));
        }
    }
}
