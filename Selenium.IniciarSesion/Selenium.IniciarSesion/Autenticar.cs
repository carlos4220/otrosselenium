﻿namespace Selenium.IniciarSesion
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;
    using System.Threading;

    public class Autenticar
    {
        public static string usuariop { get; set; }

        public static void InicioSesion(RemoteWebDriver manejador, WebDriverWait pausa, int linea, string direccion, string instalacion, string usuario, string contrasenia)
        {
            switch (linea)
            {
                case 1: /// Sitio: Administración Bi En Línea
                    manejador.Url = direccion;
                    manejador.FindElement(By.Id("txtUsuario")).SendKeys(usuario);
                    manejador.FindElement(By.Id("txtPassword")).SendKeys(contrasenia);
                    manejador.FindElement(By.Id("cmdIngresar")).Click();
                    try
                    {
                        manejador.SwitchTo().Alert().Accept();
                    }
                    catch (NoAlertPresentException)
                    {

                    }
                    break;
                case 2: /// Sitio: Cliente Bi En Línea
                    manejador.Url = direccion;
                    manejador.FindElement(By.Id("campoInstalacion")).SendKeys(instalacion);
                    manejador.FindElement(By.Id("campoUsuario")).SendKeys(usuario);
                    manejador.FindElement(By.Id("campoContrasenia")).SendKeys(contrasenia);

                    manejador.FindElement(By.Id("autenticar")).Click();

                    pausa.Until(m => m.FindElement(By.ClassName("close-reveal-modal")));
                    foreach (var item in manejador.FindElements(By.ClassName("close-reveal-modal")))
                    {
                        if (item.Displayed)
                        {
                            item.Click();
                        }
                    }
                    break;
                case 3: /// Sitio: Administración Bi Banking
                    manejador.Url = direccion;
                    manejador.FindElement(By.Id("txtUsuario")).SendKeys(usuario);
                    manejador.FindElement(By.Id("txtPassword")).SendKeys(contrasenia);
                    manejador.FindElement(By.Id("cmdIngresar")).SendKeys(Keys.Enter);
                    try
                    {
                        manejador.SwitchTo().Alert().Accept();
                        Thread.Sleep(5000);
                        //pausa.Until(m => m.SwitchTo().Alert());
                    }
                    catch (NoAlertPresentException)
                    {

                    }
                    break;
                case 4: /// Sitio: Cliente Bi Banking
                    manejador.Url = direccion;
                    manejador.FindElement(By.Id("usercode")).SendKeys(instalacion);
                    manejador.FindElement(By.Name("Usuario")).SendKeys(usuario);
                    manejador.FindElement(By.Name("Contrasena")).SendKeys(contrasenia);

                    manejador.FindElement(By.Name("Contrasena")).SendKeys(Keys.Enter);
                    System.Threading.Thread.Sleep(40000);
                    break;
            }
        }
    }
}
