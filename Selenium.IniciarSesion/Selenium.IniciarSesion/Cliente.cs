﻿namespace Selenium.IniciarSesion
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;
    using System.Threading;

    public class Cliente
    {
        /// <summary>
        /// Prueba el proceso: Seleccion Cliente
        /// </summary>
        public static void SeleccionarCliente(RemoteWebDriver manejador, WebDriverWait pausa, string cliente)
        {
            // Se detiene el hilo de trabajo ya que el servidor 72 es mas lento y no reacciona como el 55
            // Selección de Cliente
            pausa.Until(m=>m.FindElement(By.CssSelector("td:nth-child(1) > a"))).Click();
            System.Threading.Thread.Sleep(2000);
            pausa.Until(m => m.FindElement(By.Name("cmdSeleccionar"))).Click();
            System.Threading.Thread.Sleep(2000);
            pausa.Until(m=>m.FindElement(By.Id("txtNombre"))).SendKeys(cliente);
            System.Threading.Thread.Sleep(2000);
            pausa.Until(m=> m.FindElement(By.Id("cmdSeleccionar"))).Click();
            System.Threading.Thread.Sleep(2000);
            pausa.Until(m => m.FindElement(By.LinkText("Seleccionar"))).Click();
            System.Threading.Thread.Sleep(2000);
            try
            {
                manejador.SwitchTo().Alert().Accept();
            }
            catch(NoAlertPresentException ex)
            {

            }
            manejador.SwitchTo().DefaultContent();
            System.Threading.Thread.Sleep(3000);
        }
    }
}
