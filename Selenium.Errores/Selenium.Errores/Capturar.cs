﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace Selenium.Errores
{
    public class Capturar
    {
        public static void GuardarError(RemoteWebDriver manejador, string ruta)
        {
            Screenshot captura = ((ITakesScreenshot)manejador).GetScreenshot();
            captura.SaveAsFile(ruta, ScreenshotImageFormat.Jpeg);
        }
    }
}
